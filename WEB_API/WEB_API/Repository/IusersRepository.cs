﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Web;

namespace WEB_API.Repository
{
    public interface IusersRepository<user>
    {
        Task<user> GetByIdAsync(int idU);
        IQueryable<user> SearchFor(Expression<Func<user, bool>> predicate);

        IQueryable<user> GetAll();

        Task EditAsync(user entity);

        Task InsertAsync(user entity);

        Task DeleteAsync(user entity);
        Task SaveChangesAsync(user entity);
        Task<user> FindAsync(int id);


    }
}