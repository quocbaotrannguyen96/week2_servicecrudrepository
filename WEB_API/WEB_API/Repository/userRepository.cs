﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using WEB_API.Models;

namespace WEB_API.Repository
{
    public class usersRepository<user> : IusersRepository<user> where user : class
    {
        protected DbSet<user> DbSet;

        private readonly DbContext _dbContext;

        public usersRepository(DbContext dbContext)
        {
            _dbContext = dbContext;
            DbSet = _dbContext.Set<user>();
        }

        public usersRepository()
        {
        }

        public IQueryable<user> GetAll()
        {
            return DbSet;
        }

        public async Task<user> GetByIdAsync(int idU)
        {
            return await DbSet.FindAsync(idU);
        }

        public IQueryable<user> SearchFor(Expression<Func<user, bool>> predicate)
        {
            return DbSet.Where(predicate);
        }

        public async Task EditAsync(user entity)
        {
            _dbContext.Entry(entity).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();
        }

        public async Task InsertAsync(user entity)
        {

            DbSet.Add(entity);
            await _dbContext.SaveChangesAsync();
        }

        public async Task DeleteAsync(user entity)
        {
            DbSet.Remove(entity);
            await _dbContext.SaveChangesAsync();
        }
        public async Task SaveChangesAsync(user entity)
        {
            await _dbContext.SaveChangesAsync();
        }
        public async Task<user> FindAsync(int id)
        {
            return await DbSet.FindAsync(id);
        }
        public void Dispose()
        {
            if (_dbContext != null)
                _dbContext.Dispose();
        }

    }
}
