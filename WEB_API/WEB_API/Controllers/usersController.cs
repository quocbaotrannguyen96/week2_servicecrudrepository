﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using WEB_API.Models;
using WEB_API.Repository;

namespace WEB_API.Controllers
{
    public class usersController : ApiController
    {
        private testCRUDEntities db = new testCRUDEntities();
        private usersRepository<user> userRepo;
        public usersController()
        {
            testCRUDEntities db = new testCRUDEntities();
            userRepo = new usersRepository<user>(db);
        }
        // GET: api/users
        public IQueryable<user> Getusers()
        {
            return userRepo.GetAll();
        }

        // GET: api/users/5
        
        [ResponseType(typeof(user))]
        //public IHttpActionResult Getuser(int id)
        //{
        //    user user = db.users.Find(id);
        //    if (user == null)
        //    {
        //        return NotFound();
        //    }

        //    return Ok(user);
        //}
        [ResponseType(typeof(user))]
        public async Task<IHttpActionResult> Getuser(int id)
        {
            user usr = await userRepo.GetByIdAsync(id);
            if (usr == null)
            {
                return NotFound();
            }

            return Ok(usr);
        }

        // PUT: api/users/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> Putuser(int id, user usr)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != usr.id)
            {
                return BadRequest();
            }
            await userRepo.EditAsync(usr);
            try
            {
                await userRepo.SaveChangesAsync(usr);
            }
            catch (DbUpdateConcurrencyException)
            {

            }

            return StatusCode(HttpStatusCode.Created);
        }

        // POST: api/users
        [ResponseType(typeof(user))]
        public async Task<IHttpActionResult> Postuser(user usr)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            await userRepo.InsertAsync(usr);


            return CreatedAtRoute("DefaultApi", new { id = usr.id }, usr);
        }

        // DELETE: api/users/5
        [ResponseType(typeof(user))]
        public async Task<IHttpActionResult> Deleteuser(int id)
        {

            user usr = await userRepo.FindAsync(id);
            if (usr == null)
            {
                return NotFound();
            }

            await userRepo.DeleteAsync(usr);
            return Ok(usr);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                userRepo.Dispose();
            }
            base.Dispose(disposing);
        }

        
    }
}